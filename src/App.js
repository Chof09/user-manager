import React from 'react';
import UserList from './components/UserList/UserList';

function App() {
    return (
        <div className="container bg-light shadow-sm">
            <UserList />
        </div>
    );
}

export default App;