import axios from 'axios';

class DatabaseAdapter {

    constructor(baseURL) {
        this.baseURL = baseURL;
        this.axiosInstance = axios.create({
            baseURL: this.baseURL
        });
    }

    login = () => {

    }

    getUsers = () => this.axiosInstance.get('/users?delay=1');  // Delay set to see the spinner

    addUser = () => {

    }

}

const dbInstance = new DatabaseAdapter('https://reqres.in/api/');

export default dbInstance;