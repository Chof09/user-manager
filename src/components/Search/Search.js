import React from 'react';

const Search = (props) => {
    return (
        <input className="form-control mr-sm-4" type="search" placeholder="Search" aria-label="Search" />
    );
};

export default Search;