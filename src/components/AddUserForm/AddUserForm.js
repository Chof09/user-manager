import React from 'react';

import TextInput from '../FormItems/TextInput';

const AddUserForm = (props) => {
    return (
        <form>
            <TextInput
                label="First name"
                id="first-name"
                placeholder="First name"
                value=""
                changed="" />
            <TextInput
                label="Last name"
                id="last-name"
                placeholder="Last name"
                value=""
                changed="" />
            <TextInput
                label="Email"
                id="email"
                placeholder="Email"
                value=""
                changed="" />
            <TextInput
                label="Avatar URL"
                id="avatar-url"
                placeholder="Avatar URL"
                value=""
                changed="" />

            <button type="submit" className="btn btn-primary">Add User</button>
        </form>
    );
};

export default AddUserForm;