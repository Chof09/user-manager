import React, { Component } from 'react';
import styles from './UserList.module.css';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actionCreators';

import UserCard from '../UserCard/UserCard';
import Header from '../Header/Header.js';
import Modal from '../UI/Modal/Modal';
import AddUserForm from '../AddUserForm/AddUserForm';
import Spinner from '../UI/Spinner/Spinner';

class UserList extends Component {

    state = {
        modalOpened: false
    };

    componentDidMount () {
        this.props.onGetUsers();
    }

    handleModalClose = () => {
        this.setState({ modalOpened: false });
    }

    handleModalOpen = () => {
        this.setState({ modalOpened: true });
    }

    render () {

        let userList = <Spinner />;
        let errorMsg = null;

        if (this.props.usersList.length) {
            userList = <section className={ styles.UserList }>
                {this.props.usersList.map(user => (
                    <UserCard key={ user.id } avatar={ user.avatar } firstName={ user.first_name } lastName={ user.last_name } />
                ))}
            </section>;
        }
        if (this.props.errorLoading) {
            errorMsg = <p className="alert alert-danger">Problem loading users</p>
        }

        return (
            <React.Fragment>
                <Header showModal={ this.handleModalOpen } />
                <main className="row">
                    <div className="col pb-4">
                        { errorMsg || userList }
                    </div>
                </main>
                <Modal show={ this.state.modalOpened } hide={ this.handleModalClose }>
                    <AddUserForm />
                </Modal>
            </React.Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        usersList: state.users,
        errorLoading: state.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetUsers: () => dispatch(actionCreators.getUsers())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);