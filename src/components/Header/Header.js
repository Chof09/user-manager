import React from 'react';

import Search from '../Search/Search';
import AddBtn from '../AddBtn/AddBtn';

const Header = (props) => {
    return (
        <header className="row mb-4">
            <div className="col p-0">
                <div className="navbar navbar-dark bg-dark">
                    <h1 className="navbar-brand mb-0">User Manager</h1>
                    <div className="d-inline-flex">
                        <Search />
                        <AddBtn showModal={ props.showModal } />
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;