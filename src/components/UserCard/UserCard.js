import React from 'react';
import styles from './UserCard.module.css';

const UserCard = (props) => {
    return (
        <article className={`${styles.UserCard} card shadow-sm`}>
            <img className="card-img-top" src={ props.avatar } alt="Profile" />
            <div className="card-body">
                <p className="card-text">{ `${props.firstName} ${props.lastName}` }</p>
            </div>
        </article>
    );
};

export default UserCard;