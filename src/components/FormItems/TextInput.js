import React from 'react';

const TextInput = (props) => {
    return (
        <div className="form-group">
            <label htmlFor={props.id}>{props.label}</label>
            <input
                className="form-control"
                id={props.id}
                type="text"
                placeholder={props.placeholder}
                value={props.value ? props.value : ''}
                onChange={(event) => props.changed(event.target.value, props.id)} />
        </div>
    );
};

export default TextInput;