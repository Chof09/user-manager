import React from 'react';
import styles from './Modal.module.css';

const Modal = (props) => {

    const handleButtonClick = (event) => {
        event.preventDefault();
        props.hide();
    };

    return (
        <React.Fragment>
            <div className={`modal ${props.show ? styles.show : null}`}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Add new user</h5>
                            <button className="close" onClick={handleButtonClick}><i className="fas fa-times"></i></button>
                        </div>
                        <div className="modal-body">
                            {props.children}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Modal;