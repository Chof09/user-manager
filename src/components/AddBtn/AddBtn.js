import React from 'react';
import styles from './AddBtn.module.css';

const AddBtn = (props) => {

    const handleButtonClick = (event) => {
        event.preventDefault();
        props.showModal();
    };

    return (
        <button className="btn btn-outline-success my-2 my-sm-0" type="button" onClick={ handleButtonClick }>
            <i className={`${styles.iconBtn} fas fa-plus`}></i>Add User
        </button>
    );
};

export default AddBtn;