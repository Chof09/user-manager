const initialState = {
    users: [],
    error: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USERS':
            return {
                users: action.users,
                error: false
            };
        case 'GET_USERS_FAILED':
            return {
                ...state,
                error: true
            };
        default:
            return state;
    }
};

export default reducer;