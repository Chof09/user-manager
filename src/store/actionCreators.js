import db from '../db';

export const getUsers = () => {
    return (dispatch) => {
        db.getUsers()
            .then(response => {
                if (response.status === 200) {
                    dispatch({type: 'GET_USERS', users: response.data.data});
                }
            })
            .catch(error => {
                dispatch({type: 'GET_USERS_FAILED'});
                console.error(error);
            });
    };
};